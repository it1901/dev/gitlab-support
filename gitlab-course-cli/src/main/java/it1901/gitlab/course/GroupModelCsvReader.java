package it1901.gitlab.course;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import com.univocity.parsers.csv.CsvFormat;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

public class GroupModelCsvReader {

    private Predicate<String> includeTest = null;
    
    public void setIncludeTest(Predicate<String> includeTest) {
		this.includeTest = includeTest;
	}

    private Predicate<String> excludeTest = null;
    
    public void setExcludeTest(Predicate<String> excludeTest) {
		this.excludeTest = excludeTest;
	}
    
    private boolean includeStudents = true;
    
    public void setIncludeStudents(boolean includeStudents) {
    	this.includeStudents = includeStudents;
    }
    
    public Map<String, Group> readGroups(InputStream input) {
        Map<String, Group> groups = new HashMap<String, Group>();
        CsvParser csvParser = csvParser();
        csvParser.beginParsing(input);
        String[] fields = null;
        while ((fields = csvParser.parseNext()) != null) {
            int groupNum = -1;
            String studentId = null, fullName = null;
            for (String field : fields) {
                try {
                    int num = Integer.valueOf(field);
                    if (groupNum < 0 && num < 1000) {
                        groupNum = num;
                    }
                } catch (NumberFormatException e) {
                    if (! includeStudents)
                        ;
                    else if (field == null || field.length() == 0)
                        ;
                    else if (Character.isUpperCase(field.charAt(0))) {
                        if (fullName == null) {
                            fullName = field;
                        } else {
                            fullName += " " + field;
                        }
                    } else if (studentId == null) {
                        studentId = field;
                    }
                }
            }
            if (groupNum >= 0) {
                String groupId = String.valueOf(groupNum);
                if (includeTest != null && (! includeTest.test(groupId))) {
                	continue;
                }
                if (excludeTest != null && excludeTest.test(groupId)) {
                	continue;
                }
                Group group = groups.get(groupId);
                if (group == null) {
                    group = new Group(groupId);
                    groups.put(groupId, group);
                }
                if (includeStudents && studentId != null) {
                    Student student = new Student(studentId);
                    student.setFullName(fullName);
                    group.addMembers(new Member(student));
                }
            }
        }
        return groups;
    }

    protected CsvParser csvParser() {
        CsvParserSettings settings = new CsvParserSettings();
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setFormat(csvFormat());
        return new CsvParser(settings);
    }

    protected CsvFormat csvFormat() {
        CsvFormat format = new CsvFormat();
        format.setQuote('"');
        format.setDelimiter(",");
        format.setComment('#');
        return format;
    }

    public static void main(String[] args) {
        try (InputStream input = new FileInputStream(
                "/Users/hal/java/git/it1901/dev/gitlab-support/gitlab-course-maven-plugin-example/groupmembers.csv")) {
            Map<String, Group> groups = new GroupModelCsvReader().readGroups(input);
            groups.values().stream().sorted((g1, g2) -> g1.getId().compareTo(g2.getId())).forEach(System.out::println);
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}
