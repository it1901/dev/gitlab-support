package it1901.gitlab.maven;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import it1901.gitlab.cli.ListStudentProjectsTask;

/**
 * Lists student projects
 */
@Mojo(name = "list-students")
public class ListStudentsMojo extends AbstractGitlabMojo {

	@Parameter(required = true)
	private String courseStudentsProjectsOwnerPath;

	@Parameter(defaultValue = "%s")
	private String studentProjectNameFormat;

	@Parameter(property = "list-students.commitedSince")
	private String commitedSince;

	@Parameter(property = "list-students.commitedUntil")
	private String commitedUntil;

	@Override
	public void execute() throws MojoExecutionException {
		ListStudentProjectsTask.main(
			"-gitlabInstance", getGitlabInstance(),
			"-gitlabPersonalAccessToken", getGitlabPersonalAccessToken(),
			"-courseProjectPath", getMavenProject().getBuild().getDirectory(),
			"-courseStudentsProjectsOwnerPath", courseStudentsProjectsOwnerPath,
			"-studentProjectNameFormat", studentProjectNameFormat,
			"-commitedSince", commitedSince,
			"-commitedUntil", commitedUntil
		);
	}
}
