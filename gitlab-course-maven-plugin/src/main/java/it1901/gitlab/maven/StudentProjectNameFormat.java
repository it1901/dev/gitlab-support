package it1901.gitlab.maven;

import java.util.ArrayList;
import java.util.List;

import org.gitlab4j.api.models.Project;

public class StudentProjectNameFormat {

	private String format;
	
	public StudentProjectNameFormat(String format) {
		this.format = format;
	}
	
	public String getStudentName(String projectName) {
		List<String> nameParts = extractProjectNameParts(projectName);
		return (nameParts != null && nameParts.size() >= 2 ? nameParts.get(1) : null);
	}

	public String getStudentName(Project project) {
		return getStudentName(project.getName());
	}

	public String getProjectName(String studentName) {
		return String.format(format, studentName);
	}

	protected List<String> extractProjectNameParts(String projectName) {
		List<String> parts = new ArrayList<>();
		int start = 0;
		while (start < format.length()) {
			int end = format.indexOf('%', start);
			String separator = format.substring(start, end < 0 ? format.length() : end);
			int pos = projectName.indexOf(separator);
			if (pos < 0) {
				return null;
			}
			if (parts.size() > 0) {
				parts.add(projectName.substring(0, pos));
			}
			if (end >= 0) {
				parts.add(format.substring(end + 1, end + 2));
			}
			projectName = projectName.substring(separator.length());
			start = (end < 0 ? format.length() : end) + 2;
		}
		if (parts.size() % 2 == 1) {
			parts.add(projectName);
		}
		return parts;
	}
}
