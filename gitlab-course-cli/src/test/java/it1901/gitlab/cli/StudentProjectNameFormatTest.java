package it1901.gitlab.cli;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StudentProjectNameFormatTest {

	@Test
	public void testGetStudentName_varAtBeginning() {
		StudentProjectNameFormat nameFormat = new StudentProjectNameFormat("%s-TDT4100-prosjekt");
		Assertions.assertEquals("robingg", nameFormat.getStudentName("robingg-TDT4100-prosjekt"));
	}

	public void testGetStudentName_varInMiddle() {
		StudentProjectNameFormat nameFormat = new StudentProjectNameFormat("TDT4100-%s-prosjekt");
		Assertions.assertEquals("robingg", nameFormat.getStudentName("TDT4100-robingg-prosjekt"));
	}
	public void testGetStudentName_varAtEnd() {
		StudentProjectNameFormat nameFormat = new StudentProjectNameFormat("TDT4100-prosjekt-%s");
		Assertions.assertEquals("robingg", nameFormat.getStudentName("TDT4100-prosjekt-robingg"));
	}
}
