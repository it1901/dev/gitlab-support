package it1901.gitlab.cli;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Map;

import it1901.gitlab.course.Group;
import it1901.gitlab.graphql.GraphQlQueryHelper;
import it1901.gitlab.graphql.IssuesCsvWriter;
import it1901.gitlab.graphql.ProjectQuery;
import it1901.gitlab.graphql.ProjectQuery.Project;
import it1901.gitlab.repo.RepoNameProvider;
import picocli.CommandLine;
/**
 * Reports issues' notes as CSV
 */
public class ReportGroupIssuesNotesTask extends AbstractGitlabGroupTask {

    @Override
    public void run() throws TaskException {
        Map<String, Group> groups = readGroupsCsv(false);
        RepoNameProvider repoNameProvider = getRepoNameProvider();
        GraphQlQueryHelper qlHelper = new GraphQlQueryHelper(getGitlabInstance(), getGitlabPersonalAccessToken());
        for (String groupId : groups.keySet()) {
            String repoName = repoNameProvider.getRepoName(groupId);
            String query =
                "project(fullPath:\"" + getCourseGroupsGroupsOwnerPath() + File.separator + repoName + File.separator + repoName + "\") { " +
                        "  issues { " + 
                        "    nodes { " +
                        "      iid title description createdAt " + 
                        "      author { username } " + 
                        "      assignees { nodes { username } } " + 
                        "      participants { nodes { username } } " + 
                        "      notes { nodes { createdAt author { username } body } }" + 
                        "    } " +
                        "  } " +
                        "}";
            getLog().debug("Query for " + groupId + " in " + repoName + ": " + query);
            Project project = null;
			try {
				project = qlHelper.executeGraphQl(query, ProjectQuery.class).data.project;
			} catch (Exception e) {
				getLog().info("Exception getting info for " + groupId + " in " + repoName + ": " + e);
			}
            if (project == null) {
            	getLog().info("Couldn't get info for " + groupId + " in " + repoName);
                continue;
            }
            IssuesCsvWriter csvWriter = new IssuesCsvWriter(project.issues.nodes);
            try {
                Path dir = ensureTargetDirectory("gitlab");
                try (OutputStream output = new FileOutputStream(dir.resolve(repoName + "-issues-notes.csv").toFile())) {
                    csvWriter.writeCsv(output);
                    getLog().info("Wrote csv for group " + groupId + " in " + repoName);
                } catch (IOException ioe) {
                    getLog().info("Couldn't write csv for group " + groupId + " in " + repoName + ": " + ioe.getMessage());
                }
            } catch (IOException ioe) {
                throw new TaskException("Couldn't create build directory for group " + groupId + ": " + ioe.getMessage());
            }
        }
    }
    
	public static void main(String... args) {
		new CommandLine(new ReportGroupIssuesNotesTask()).execute(args);
	}
}
