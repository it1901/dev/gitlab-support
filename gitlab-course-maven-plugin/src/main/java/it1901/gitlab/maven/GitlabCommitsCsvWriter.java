package it1901.gitlab.maven;

import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.CommitStats;

import com.univocity.parsers.csv.CsvWriterSettings;

import it1901.gitlab.graphql.CsvWriter;

public class GitlabCommitsCsvWriter extends CsvWriter {

    private Collection<Commit> commits;
    
    public GitlabCommitsCsvWriter(Collection<Commit> commits) {
        this.commits = new ArrayList<Commit>(commits);
    }
    public GitlabCommitsCsvWriter(Commit... commits) {
        this.commits = Arrays.asList(commits);
    }        
    
    private String[] headers = {
        "commitId", "userName", "commitDate", "additionsCount", "deletionsCount","title", "message"
    };
    
    @Override
    protected String[] getHeaders() {
        return headers;
    }
    
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");

    @Override
    protected void writeCsvRows(OutputStream output) {
        for (Commit commit: commits) {
          String[] entries = new String[headers.length];
          entries[0] = commit.getId();
          entries[1] = commit.getCommitterEmail();
          Date commitDate = commit.getCommittedDate();
          if (commitDate == null) {
        	  commitDate = commit.getCreatedAt();
          }
          entries[2] = (commitDate != null ? dateFormat.format(commitDate) : "");
          CommitStats stats = commit.getStats();
          entries[3] = String.valueOf(stats != null ? stats.getAdditions() : -1);
          entries[4] = String.valueOf(stats != null ? stats.getDeletions() : -1);
          entries[5] = commit.getTitle();
          entries[6] = commit.getMessage();
          getCsvWriter().writeRow(entries);
        }
    }
}
