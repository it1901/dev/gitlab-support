package it1901.gitlab.repo;

public class FormatRepoNameProvider implements RepoNameProvider {

	private final String repoNameFormat;
	
	public FormatRepoNameProvider(String repoNameFormat) {
		this.repoNameFormat = repoNameFormat;
	}

	@Override
	public String getRepoName(String groupId) {
		Object id = groupId;
		try {
			// try converting to integer, in case id is num-based and should be padded
			id = Integer.valueOf(groupId);
		} catch (NumberFormatException e) {
		}
		return String.format(repoNameFormat, id);
	};
}
