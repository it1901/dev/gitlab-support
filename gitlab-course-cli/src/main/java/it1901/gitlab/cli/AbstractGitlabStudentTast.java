package it1901.gitlab.cli;

import picocli.CommandLine;

public abstract class AbstractGitlabStudentTast extends AbstractGitlabCourseTask {

	@CommandLine.Option(names = "-courseStudentsProjectsOwnerPath", description = "gitlab path to group containing student projects")
	private String courseStudentsProjectsOwnerPath;

	public String getCourseStudentsProjectsOwnerPath() {
		return courseStudentsProjectsOwnerPath;
	}
	
	@CommandLine.Option(names = "-studentProjectNameFormat", description = "string format for student project name")
	private String studentProjectNameFormat = "%s";

	public String getStudentProjectNameFormat() {
		return studentProjectNameFormat;
	}
	
	public void setStudentProjectNameFormat(String studentProjectNameFormat) {
		this.studentProjectNameFormat = studentProjectNameFormat;
	}
}
