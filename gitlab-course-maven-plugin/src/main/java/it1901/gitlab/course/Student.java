package it1901.gitlab.course;

public class Student {

    private final String userName;
    private String fullName;    
    
    public Student(String userName) {
        this.userName = userName;
    }
    
    @Override
    public String toString() {
        return String.format("[Student %s %s]", getId(), getFullName());
    }
    
    public String getId() {
        return userName;
    }
    
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
