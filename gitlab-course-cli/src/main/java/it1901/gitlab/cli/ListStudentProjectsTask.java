package it1901.gitlab.cli;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Vector;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.gitlab4j.api.CommitsApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;

import picocli.CommandLine;

/**
 * Lists student projects
 */
public class ListStudentProjectsTask extends AbstractGitlabStudentTast {
	
	@CommandLine.Option(names = "-commitedSince", description = "limits student projects to those with commits since this date")
	private String commitedSince;

	@CommandLine.Option(names = "-commitedUntil", description = "limits student projects to those with commits before this date")
	private String commitedUntil;

	private boolean hasCommits(Project project, Date since, Date until) {
		if (since != null || until != null) {
			CommitsApi commitsApi = getCommitsApi();
			try {
				return commitsApi.getCommitsStream(project, null, since, until).findAny().isPresent();
//			long count = commitsApi.getCommits(project, null, since, until).stream().count();
//			System.out.println(count + " commits found for " + project.getName() + " since " + since + " and until " + until);
//			return count > 0; // findAny().isPresent();
			} catch (GitLabApiException e) {
			}
		}
		return true;
	}

	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	private Date string2Date(String dateString) {
		try {
			return dateFormat.parse(dateString);
		} catch (ParseException e) {
			return null;
		}
	}

	@Override
	public void run() {
		getLog().info("Getting project names in " + getCourseStudentsProjectsOwnerPath());
		try {
			Stream<Project> projectStream = getGroupApi().getProjects(getCourseStudentsProjectsOwnerPath(), 50).lazyStream();
			Date since = (isDefined(commitedSince) ? string2Date(commitedSince) : null);
			Date until = (isDefined(commitedUntil) ? string2Date(commitedUntil) : null);
			// thread safe
			final Vector<Project> projects = new Vector<>();
			if (since != null || until != null) {
				int nThreads = 10;
				Executor executor = (since != null || until != null ? Executors.newFixedThreadPool(nThreads) : null);
				AtomicInteger remaining = new AtomicInteger();
				projectStream.forEach(project -> {
					remaining.incrementAndGet();
					// schedule task for adding project if it has commits
					executor.execute(() -> {
						if (hasCommits(project, since, until)) {
							projects.add(project);
						}
						// decrement and signal that the counter has changed
						synchronized (remaining) {
							remaining.decrementAndGet();
							remaining.notify();
						}
					});
				});
				// wait for remaining to become zero
				while (true) {
					synchronized (remaining) {
						if (remaining.get() == 0) {
							break;
						}
						remaining.wait();
					}
				}
			} else {
				projectStream.forEach(projects::add);
			}
			getLog().info("Found " + projects.size() + " student projects in " + getCourseStudentsProjectsOwnerPath());
			StudentProjectNameFormat projectNameFormat = new StudentProjectNameFormat(getStudentProjectNameFormat());
			projects.stream().map(projectNameFormat::getStudentName).filter(Objects::nonNull).forEach(System.out::println);
		} catch (final Exception e) {
			throw new TaskException("Exception getting student project names", e);
		}
	}

	public static void main(final String... args) {
		new CommandLine(new ListStudentProjectsTask()).execute(args);
	}
}
