package it1901.gitlab.maven;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import it1901.gitlab.cli.GroupGitRepoTask;

/**
 * Reports issues' notes as CSV
 */
@Mojo(name = "group-git-repo")
public class GroupGitRepoMojo extends AbstractGitRepoMojo {

	@Parameter(required = true)
	private String groupsCsvPath;

	@Parameter(required = true)
	private String courseGroupsGroupsOwnerPath;

	@Override
	public void execute() throws MojoExecutionException {
		GroupGitRepoTask.main(
			"-gitlabInstance", getGitlabInstance(),
			"-gitlabPersonalAccessToken", getGitlabPersonalAccessToken(),
			"-courseProjectPath", getMavenProject().getBuild().getDirectory(),
			"-groupsCsvPath", groupsCsvPath,
			"-courseGroupsGroupsOwnerPath", courseGroupsGroupsOwnerPath
			);		
	}
}
