package it1901.gitlab.graphql;

import java.io.OutputStream;

import com.univocity.parsers.csv.CsvWriterSettings;

public abstract class CsvWriter {

    protected abstract String[] getHeaders();
    
    protected abstract void writeCsvRows(OutputStream output);

    private com.univocity.parsers.csv.CsvWriter csvWriter;

    public com.univocity.parsers.csv.CsvWriter getCsvWriter() {
		return csvWriter;
	}
    
    public void writeCsvStart(OutputStream output) {
		CsvWriterSettings settings = createSettings();
		csvWriter = new com.univocity.parsers.csv.CsvWriter(output, settings);
		csvWriter.writeHeaders(getHeaders());
    }
    
    public void writeCsvEnd(OutputStream output) {
    	if (csvWriter != null) {
    		csvWriter.flush();
    		csvWriter.close();
    		csvWriter = null;
    	}
    }
    
    public void writeCsv(OutputStream output) {
      try {
    	  writeCsvStart(output);
          writeCsvRows(output);
      } finally {
        writeCsvEnd(output);
      }
    }

    protected CsvWriterSettings createSettings() {
      return initSettings(new CsvWriterSettings());
    }
    
    protected CsvWriterSettings initSettings(CsvWriterSettings settings) {
        // Sets the character sequence to write for the values that are null.
        settings.setNullValue("");
        settings.getFormat().setDelimiter(',');
        // use defaults, so we can read using defaults
//        settings.getFormat().setQuote('"');
//        settings.getFormat().setQuoteEscape('\\');
        settings.getFormat().setLineSeparator("\n");
        settings.setIgnoreLeadingWhitespaces(true);
        settings.setIgnoreTrailingWhitespaces(true);
        settings.setSkipEmptyLines(true);
        return settings;
    }
}
