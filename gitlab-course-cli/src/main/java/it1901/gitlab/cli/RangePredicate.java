package it1901.gitlab.cli;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class RangePredicate<T extends Comparable<T>> implements Predicate<T> {

	private List<T> ranges = new ArrayList<>();

	public void addItem(T item) {
		ranges.add(item);
		ranges.add(item);
	}

	public void addRange(T lower, T upper) {
		ranges.add(lower);
		ranges.add(upper);
	}

	private final static String rangeSeparator = "-";

	public static <T extends Comparable<T>> RangePredicate<T> of(Function<String, T> converter, String... itemsAndRanges) {
		RangePredicate<T> pred = new RangePredicate<>();
		for (String itemOrRange : itemsAndRanges) {
			int pos = itemOrRange.indexOf(rangeSeparator);
			if (pos < 0) {
				pred.addItem(converter.apply(itemOrRange));
			} else {
				T lower = (pos > 0 ? converter.apply(itemOrRange.substring(pos + rangeSeparator.length())) : null);
				T upper = (pos < itemOrRange.length() - rangeSeparator.length() ? converter.apply(itemOrRange.substring(0, pos)) : null);
				pred.addRange(lower, upper);
			}
		}
		return pred;
	}
	
	@Override
	public boolean test(T t) {
		for (int i = 0; i < ranges.size(); i += 2) {
			T lower = ranges.get(i); 
			T upper = ranges.get(i + 1);
			if (t.compareTo(lower) >= 0 && t.compareTo(upper) <= 0) {
				return true;
			}
		}
		return false;
	}
}
