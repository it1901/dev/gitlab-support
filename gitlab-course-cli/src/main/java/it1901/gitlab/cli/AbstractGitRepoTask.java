package it1901.gitlab.cli;

import java.nio.file.Files;
import java.nio.file.Path;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import picocli.CommandLine;

public abstract class AbstractGitRepoTask extends AbstractGitlabCourseTask {

	@CommandLine.Option(names = "-groupsGitLocalFolder", description = "relative path to target folder for group repos")
	private String groupsGitLocalFolder;

	protected Path getLocalGitRepoDir(String repoName) {
		return getCourseProjectTargetPath().resolve(groupsGitLocalFolder).resolve(repoName);
	}

	private CredentialsProvider credentialsProvider = null;

	public CredentialsProvider getCredentialsProvider() {
		if (credentialsProvider == null) {
			credentialsProvider = new UsernamePasswordCredentialsProvider("oauth2", getGitlabPersonalAccessToken());
		}
		return credentialsProvider;
	}

	protected Boolean ensureGitRepo(String repoName, String repoUrl, String branch) {
		Path targetDir = getLocalGitRepoDir(repoName);
		if (Files.exists(targetDir)) {
			return null;
		}
		if (repoUrl != null) {
			try (Git repo = Git.cloneRepository()
					.setURI(repoUrl)
					.setDirectory(targetDir.toFile())
					.setCloneAllBranches(true)
					.setCredentialsProvider(getCredentialsProvider())
					.call()) {
				getLog().info("Cloned " + repoUrl + " to " + targetDir);
				if (branch != null) {
					repo.checkout()
						.setName(branch)
						.call();
					getLog().info("Checked out " + branch + " to " + targetDir);
				}
				return true;
			} catch (IllegalStateException | GitAPIException e) {
				System.err.println(e);
				return false;
			}
		} else {
			try (Git repo = Git.init()
					.setDirectory(targetDir.toFile())
					.call()) {				
				System.out.println("Initialized repo at " + targetDir);
				return true;
			} catch (IllegalStateException | GitAPIException e) {
				System.err.println(e);
				return false;
			}
		}
    }
}
