package it1901.gitlab.cli;

import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.Edit;
import org.eclipse.jgit.diff.EditList;

public class DiffSizeMeasureComputer implements DiffMeasureComputer {

	@Override
	public int computeDiffMeasure(DiffFormatter diffFormatter, DiffEntry diff) throws Exception {
		EditList edits = diffFormatter.toFileHeader(diff).toEditList();
		int lineCount = 0;
		for (Edit edit : edits) {
			int editedLineCount = Math.max(edit.getLengthA(), edit.getLengthB());
			lineCount += editedLineCount;
		}
		return lineCount;
	}
}
