package it1901.gitlab.graphql;

import org.gitlab4j.api.utils.JacksonJson;

public class JsonData {

    @Override
    public String toString() {
        return JacksonJson.toJsonString(this);
    }
}
