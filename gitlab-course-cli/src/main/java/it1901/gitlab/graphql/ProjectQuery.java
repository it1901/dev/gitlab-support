package it1901.gitlab.graphql;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.gitlab4j.api.models.Assignee;
import org.gitlab4j.api.models.Author;
import org.gitlab4j.api.models.Note;
import org.gitlab4j.api.models.Participant;

public class ProjectQuery {

    public QueryData data;

    public static class QueryData {
        public Project project;
    }

    public static class Project extends JsonData {
        public ProjectIssues issues;
    }
    
    public static class ProjectIssues {
        public List<Issue> nodes;
    }
        
    public static class Issue {
        public int iid;
        public String title;
        public String description;
        public String createdAt;
        public Author author;
        public IssueAssignees assignees;
        public IssueParticipants participants;
        public IssueNotes notes;
    }

    public static class IssueAssignees {
        public List<Assignee> nodes;
    }

    public static class IssueNotes {
        public List<Note> nodes;
    }
    
    public static class IssueParticipants {
        public List<Participant> nodes;
    }
    
    public static void main(String[] args) {
        GraphQlQueryHelper qlHelper = new GraphQlQueryHelper("https://gitlab.stud.idi.ntnu.no", "xsFHWzT5JExs7xSx8Xzm");
        String query =
                "project(fullPath:\"it1901/groups-2020/gr2005/gr2005\") { " +
                "  issues { " + 
                "    nodes { " +
                "      iid title description createdAt " + 
                "      author { username } " + 
                "      assignees { nodes { username } } " + 
                "      participants { nodes { username } } " + 
                "      notes { nodes { createdAt author { username } body } }" + 
                "    } " +
                "  } " +
                "}";
        Project project = qlHelper.executeGraphQl(query, ProjectQuery.class).data.project;
//        System.out.println(project);
        IssuesCsvWriter csvWriter = new IssuesCsvWriter(project.issues.nodes);
        try (OutputStream output = System.out) { // new FileOutputStream("/Users/hal/java/git/it1901/dev/gitlab-support/gitlab-course-maven-plugin-example/gr2005-issues.csv")) {
            csvWriter.writeCsv(output);
        } catch (IOException ioe) {
            System.err.println(ioe);
        }
    }
}
