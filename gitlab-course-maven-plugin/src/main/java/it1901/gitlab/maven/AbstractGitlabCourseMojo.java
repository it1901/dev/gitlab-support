package it1901.gitlab.maven;

import it1901.gitlab.repo.RepoNameProvider;

public abstract class AbstractGitlabCourseMojo extends AbstractGitlabMojo {

    private RepoNameProvider groupRepoNameProvider;
    
    public AbstractGitlabCourseMojo() {
    }
    
    public RepoNameProvider getGroupRepoNameProvider() {
        return groupRepoNameProvider;
    }
    
    public void setGroupRepoNameProvider(RepoNameProvider groupRepoNameProvider) {
        this.groupRepoNameProvider = groupRepoNameProvider;
    }
}
