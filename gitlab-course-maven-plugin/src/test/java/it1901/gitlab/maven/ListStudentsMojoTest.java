package it1901.gitlab.maven;

import java.io.File;

import org.apache.maven.plugin.testing.AbstractMojoTestCase;

public class ListStudentsMojoTest extends AbstractMojoTestCase {
	
	private ListStudentsMojo mojo = null;

	protected void setUp() throws Exception {
        super.setUp();
        File pom = getTestFile("src/test/resources/unit/tdt4100-2021/pom.xml");
        assertNotNull(pom);
        assertTrue(pom.exists());
        try {
        	mojo = (ListStudentsMojo) lookupMojo("list-students", pom);
        } catch (Exception e) {
        	fail(e.getMessage());
        }
        assertNotNull(mojo);
    }
	
	protected void tearDown() throws Exception {
        super.tearDown();
    }
	
	public void testIgnore() {
	}
}
