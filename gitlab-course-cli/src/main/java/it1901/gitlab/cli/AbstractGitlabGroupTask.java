package it1901.gitlab.cli;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Map;

import it1901.gitlab.course.Group;
import it1901.gitlab.course.GroupModelCsvReader;
import it1901.gitlab.repo.FormatRepoNameProvider;
import it1901.gitlab.repo.RepoNameProvider;
import it1901.gitlab.repo.TimeBasedRepoNameProvider;
import picocli.CommandLine;
import picocli.CommandLine.Option;

/**
 * Lists student projects
 */
public abstract class AbstractGitlabGroupTask extends AbstractGitlabCourseTask {

	@CommandLine.Option(names = "-courseGroupsGroupsOwnerPath", description = "path to owner group of group groups")
	private String courseGroupsGroupsOwnerPath;

	public String getCourseGroupsGroupsOwnerPath() {
		return courseGroupsGroupsOwnerPath;
	}

	@CommandLine.Option(names = "-groupsCsvPath", description = "relative path to cvs with group data")
	private String groupsCsvPath;

	public String getGroupsCsvPath() {
		return groupsCsvPath;
	}

	@CommandLine.Option(names = "-includeGroupIds", description = "exclude other when reading from groups csv")
	private String includeGroupIds;

	public String getIncludeGroupIds() {
		return includeGroupIds;
	}

	@CommandLine.Option(names = "-excludeGroupIds", description = "exclude these when reading from groups csv")
	private String excludeGroupIds;

	public String getExcludeGroupIds() {
		return includeGroupIds;
	}

	public Map<String, Group> readGroupsCsv(boolean includeStudents) {
		Path csvPath = getCourseProjectPath().resolve(getGroupsCsvPath());
		Map<String, Group> groups = readGroupsCsv(csvPath, includeGroupIds, excludeGroupIds, includeStudents);
		return groups;
	}

	public static Map<String, Group> readGroupsCsv(Path groupsCsvPath, String includeGroupIds, String excludeGroupIds, boolean includeStudents) {
		GroupModelCsvReader groupModelCsvReader = new GroupModelCsvReader();
		if (isDefined(includeGroupIds)) {
			groupModelCsvReader.setIncludeTest(RangePredicate.of(s -> s, includeGroupIds.split(",")));
		}
		if (isDefined(excludeGroupIds)) {
			groupModelCsvReader.setExcludeTest(RangePredicate.of(s -> s, excludeGroupIds.split(",")));
		}
		groupModelCsvReader.setIncludeStudents(includeStudents);
		try (InputStream input = new FileInputStream(groupsCsvPath.toFile())) {
			Map<String, Group> groups = groupModelCsvReader.readGroups(input);
			return groups;
		} catch (IOException ioe) {
			throw new TaskException("Couldn't read " + groupsCsvPath + ": " + ioe.getMessage());
		}
	}

	@Option(names = "-repoNameFormat", description = "format string for repo name, only argument is groupId")
	private String repoNameFormat;

	private RepoNameProvider repoNameProvider = null;

	public RepoNameProvider getRepoNameProvider() {
		if (repoNameProvider == null) {
			repoNameProvider = (isDefined(repoNameFormat) ? new FormatRepoNameProvider(repoNameFormat) : new TimeBasedRepoNameProvider());
		}
		return repoNameProvider;
	}
}
