package it1901.gitlab.repo;

import java.time.LocalDate;

public class TimeBasedRepoNameProvider implements RepoNameProvider {

    private String timeFormat = "%ty";
    private String formatFormatFormat = "gr%s%%s";
    private int groupIdWidth = 2;
    private char groupIdPadding = '0';
    
    private LocalDate now;

    public TimeBasedRepoNameProvider(LocalDate now) {
    	this.now = now;
    }
    public TimeBasedRepoNameProvider() {
    	this(LocalDate.now());
    }

    public void setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
    }
    
    public void setFormatFormatFormat(String formatFormatFormat) {
        this.formatFormatFormat = formatFormatFormat;
    }
    
    public void setGroupIdWidth(int groupIdWidth) {
        this.groupIdWidth = groupIdWidth;
    }
    
    public void setGroupIdPadding(char groupIdPadding) {
        this.groupIdPadding = groupIdPadding;
    }

    @Override
    public String getRepoName(String id) {
        while (id.length() < groupIdWidth) {
            id = groupIdPadding + id;
        }
        String time = String.format(timeFormat, now);
        String format = String.format(formatFormatFormat, time);
        return String.format(format, id);
    }
    
    public static void main(String[] args) {
        System.out.println(new TimeBasedRepoNameProvider().getRepoName("89"));
    }
}
