package it1901.gitlab.course;

public class Member {

    private final Student student;
    
    public Member(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return String.format("[Member %s %s]", student.getId(), student.getFullName());
    }
}
