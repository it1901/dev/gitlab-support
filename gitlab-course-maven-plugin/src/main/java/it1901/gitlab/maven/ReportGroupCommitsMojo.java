package it1901.gitlab.maven;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import it1901.gitlab.cli.ReportGroupCommitsTask;

/**
 * Reports issues' notes as CSV
 */
@Mojo(name = "report-group-commits")
public class ReportGroupCommitsMojo extends AbstractGitlabMojo {
    
    @Parameter(required = true)
    private String groupsCsvPath;

    @Parameter(required = true)
    private String courseGroupsGroupsOwnerPath;

	@Parameter(property = "report-commits.commitedSince")
	private String commitedSince;

	@Parameter(property = "report-commits.commitedUntil")
	private String commitedUntil;
    
    @Override
    public void execute() throws MojoExecutionException {
    	ReportGroupCommitsTask.main(
			"-gitlabInstance", getGitlabInstance(),
			"-gitlabPersonalAccessToken", getGitlabPersonalAccessToken(),
			"-courseProjectPath", getMavenProject().getBuild().getDirectory(),
			"-groupsCsvPath", groupsCsvPath,
			"-courseGroupsGroupsOwnerPath", courseGroupsGroupsOwnerPath,
			"-commitedSince", commitedSince,
			"-commitedUntil", commitedUntil
		);
    }
}
