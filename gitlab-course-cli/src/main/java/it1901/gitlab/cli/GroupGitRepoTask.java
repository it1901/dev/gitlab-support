package it1901.gitlab.cli;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.util.io.NullOutputStream;
import org.gitlab4j.api.GitLabApiException;

import it1901.gitlab.course.Group;
import it1901.gitlab.repo.RepoNameProvider;
import it1901.gitlab.repo.TimeBasedRepoNameProvider;
import picocli.CommandLine;

/**
 * Reports issues' notes as CSV
 */
public class GroupGitRepoTask extends AbstractGitRepoTask {

	@CommandLine.Option(names = "-courseGroupsGroupsOwnerPath", description = "path to owner group of group groups")
	private String courseGroupsGroupsOwnerPath;

	public String getCourseGroupsGroupsOwnerPath() {
		return courseGroupsGroupsOwnerPath;
	}

	@CommandLine.Option(names = "-groupsCsvPath", description = "relative path to cvs with group data")
	private String groupsCsvPath;

	public String getGroupsCsvPath() {
		return groupsCsvPath;
	}

	@CommandLine.Option(names = "-includeGroupIds", description = "exclude other when reading from groups csv")
	private String includeGroupIds;

	public String getIncludeGroupIds() {
		return includeGroupIds;
	}

	@CommandLine.Option(names = "-excludeGroupIds", description = "exclude these when reading from groups csv")
	private String excludeGroupIds;

	public String getExcludeGroupIds() {
		return includeGroupIds;
	}

	private DiffSizeMeasureComputer diffMeasureComputer = new DiffSizeMeasureComputer();

	@Override
	public void run() throws TaskException {
		Path csvPath = getCourseProjectPath().resolve(getGroupsCsvPath());
		Map<String, Group> groups = AbstractGitlabGroupTask.readGroupsCsv(csvPath, includeGroupIds, excludeGroupIds, false);
		RepoNameProvider repoNameProvider = new TimeBasedRepoNameProvider(LocalDate.now().minusMonths(6));
		Collection<String> failedRepos = new ArrayList<>();
		for (String groupId : groups.keySet()) {
			String repoName = repoNameProvider.getRepoName(groupId);
			String repoPath = courseGroupsGroupsOwnerPath + File.separator + repoName + File.separator + repoName;
			String repoUrl = getGitlabInstance() + File.separator + repoPath + ".git";
			String branch = "origin/master";
			try {
				branch = getProjectApi().getProject(repoPath).getDefaultBranch();
			} catch (GitLabApiException e) {
				getLog().warn("Couldn't get default branch for " + repoPath + ", using " + branch);
			}
			Boolean ensured = ensureGitRepo(repoName, repoUrl, branch);
			if (Boolean.FALSE.equals(ensured)) {
				failedRepos.add(repoName);
			} else {
				GitCommitsCsvWriter csvWriter = new GitCommitsCsvWriter();
				csvWriter.setDiffMeasureComputer(diffMeasureComputer);
				try {
					File file = ensureTargetDirectory("gitlab").resolve(repoName + "-file-diffs.csv").toFile();
					getLog().info("Writing file diffs to " + file.getName() + " @ " + file.getParent());
					try (OutputStream csvOutput = new FileOutputStream(file)) {
						try (Git repo = Git.open(getLocalGitRepoDir(repoName).toFile())) {
							// repo.pull().setCredentialsProvider(getCredentialsProvider()).call();
							csvWriter.writeCsvStart(csvOutput);
							for (RevCommit revCommit : repo.log().call()) {
								if (revCommit.getParentCount() > 1) {
									// merge
								} else if (revCommit.getParentCount() > 0) {
									// code based on https://www.codeaffine.com/2016/06/16/jgit-diff/
									try (DiffFormatter diffFormatter = new DiffFormatter(NullOutputStream.INSTANCE)) {
										diffFormatter.setRepository(repo.getRepository());
										AbstractTreeIterator oldTree = getTreeIterator(repo.getRepository(),
												revCommit.getParent(0));
										AbstractTreeIterator newTree = getTreeIterator(repo.getRepository(), revCommit);
//										diffFmt.setPathFilter(pathFilter);
										diffFormatter.setContext(1);
										List<DiffEntry> diffs = diffFormatter.scan(oldTree, newTree);
										csvWriter.writeCsvRows(groupId, revCommit, diffs, diffFormatter, csvOutput);
									}
								}
							}
						} catch (IOException | GitAPIException e) {
						} finally {
							csvWriter.writeCsvEnd(csvOutput);
						}
					}
				} catch (IOException e) {
				}
			}
		}
		getLog().warn("Failed repos: " + failedRepos);
	}

	private AbstractTreeIterator getTreeIterator(Repository repo, RevCommit revCommit) {
		try (RevWalk walk = new RevWalk(repo); ObjectReader reader = repo.newObjectReader()) {
			return new CanonicalTreeParser(null, reader, revCommit.getTree().getId());
		} catch (IOException e) {
		}
		return null;
	}
	
	public static void main(final String... args) {
		new CommandLine(new GroupGitRepoTask()).execute(args);
	}
}
