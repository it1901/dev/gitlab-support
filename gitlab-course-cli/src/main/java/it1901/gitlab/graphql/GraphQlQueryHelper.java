package it1901.gitlab.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class GraphQlQueryHelper {

    private final String gitlabInstance;
    private final String gPAT;

    public GraphQlQueryHelper(String gitlabInstance, String gPAT) {
        this.gitlabInstance = gitlabInstance;
        this.gPAT = gPAT;
    }
    
    public String getGitlabInstance() {
        return gitlabInstance;
    }

    public String getGraphQlPath() {
        return "/api/graphql";
    }

    public String getGraphQlEndpoint() {
        return gitlabInstance + getGraphQlPath();
    }
    
    private static String quote(String json) {
        return "\"" + json.replaceAll("\"", "\\\\\"") + "\"";
    }

    public String prepareQuery(String query) {
        if (! query.startsWith("{")) {
            query = "{ " + query + " }";
        }
        int pos = query.indexOf("query");
        if (pos < 0) {
            query = "query " + query;
        }
        return query;
    }

    public String requestBody(String query) {
        return "{ \"query\": " + quote(query) + " }";
    }
    
    private String postRequest(String query) throws RuntimeException {
        Client client = ClientBuilder.newClient();
        String body = requestBody(query);
        Response response = client.target(gitlabInstance).path(getGraphQlPath())
                .request(MediaType.APPLICATION_JSON)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + gPAT)
                .post(Entity.entity(body, MediaType.APPLICATION_JSON));
        return response.readEntity(String.class);
    }
    
    public String executeGraphQl(String query) throws RuntimeException {
        try {
            query = prepareQuery(query);
            return postRequest(query);
        } catch (Exception e) {
            throw new RuntimeException("Exception executing " + query, e);
        }
    }

    private ObjectMapper objectMapper = new ObjectMapper();
    
    public <T> T executeGraphQl(String query, Class<T> resultClass) {
        try {
            query = prepareQuery(query);
            String response = postRequest(query);
            return objectMapper.readValue(response, resultClass);
        } catch (Exception e) {
            throw new RuntimeException("Exception executing " + query, e);
        }
    }
    
    public static void main(String[] args) {
        System.out.println(quote("fullPath:\"it1901/groups-2020/gr2005/gr2005\""));
    }
}
