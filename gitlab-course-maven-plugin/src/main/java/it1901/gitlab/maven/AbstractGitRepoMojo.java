package it1901.gitlab.maven;

import java.io.File;

import org.apache.maven.plugins.annotations.Parameter;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

public abstract class AbstractGitRepoMojo extends AbstractGitlabMojo {

	@Parameter(required = true)
	private String groupsGitLocalFolder;

	protected File getLocalGitRepoDir(String repoName) {
		return new File(getMavenProject().getBuild().getDirectory(), groupsGitLocalFolder + File.separator + repoName);
	}

	private CredentialsProvider credentialsProvider = null;

	public CredentialsProvider getCredentialsProvider() {
		if (credentialsProvider == null) {
			credentialsProvider = new UsernamePasswordCredentialsProvider("oauth2", getGitlabPersonalAccessToken());
		}
		return credentialsProvider;
	}

	protected Boolean ensureGitRepo(String repoName, String repoUrl, String branch) {
		File targetDir = getLocalGitRepoDir(repoName);
		if (targetDir.exists()) {
			return null;
		}
		if (repoUrl != null) {
			try (Git repo = Git.cloneRepository()
					.setURI(repoUrl)
					.setDirectory(targetDir)
					.setCloneAllBranches(true)
					.setCredentialsProvider(getCredentialsProvider())
					.call()) {
				getLog().info("Cloned " + repoUrl + " to " + targetDir);
				if (branch != null) {
					repo.checkout()
						.setName(branch)
						.call();
					getLog().info("Checked out " + branch + " to " + targetDir);
				}
				return true;
			} catch (IllegalStateException | GitAPIException e) {
				System.err.println(e);
				return false;
			}
		} else {
			try (Git repo = Git.init()
					.setDirectory(targetDir)
					.call()) {				
				System.out.println("Initialized repo at " + targetDir);
				return true;
			} catch (IllegalStateException | GitAPIException e) {
				System.err.println(e);
				return false;
			}
		}
    }
}
