package it1901.gitlab.course;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class Group {

    private final String id;

    private Collection<Member> members = new ArrayList<Member>();
    
    public Group(String id, Member... members) {
        this.id = id;
        addMembers(members);
    }
    
    @Override
    public String toString() {
        return String.format("[Group %s with %s members]", getId(), members.size());
    }
 
    public String getId() {
        return id;
    }
    
    public void addMembers(Member... members) {
        addMembers(Arrays.asList(members));
    }

    public void addMembers(Collection<Member> members) {
        this.members.addAll(members);
    }
}

