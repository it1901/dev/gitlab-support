package it1901.gitlab.maven;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.gitlab4j.api.CommitsApi;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GroupApi;
import org.gitlab4j.api.IssuesApi;
import org.gitlab4j.api.ProjectApi;

public abstract class AbstractGitlabMojo extends AbstractMojo {
	/**
	 * URL of gitlab instance.
	 *
	 * @parameter
	 * @required
	 */
	@Parameter(required = true)
	private String gitlabInstance;

	public String getGitlabInstance() {
		return gitlabInstance;
	}

	/**
	 * Personal Access Token (very secret, don't share)
	 */
	@Parameter(required = true)
	private String gitlabPersonalAccessToken;

	public String getGitlabPersonalAccessToken() {
		return gitlabPersonalAccessToken;
	}

	private GitLabApi gitLabApi;

	protected GitLabApi getGitlabApi() {
		if (gitLabApi == null) {
			gitLabApi = new GitLabApi(gitlabInstance, gitlabPersonalAccessToken);
		}
		return gitLabApi;
	}

	private ProjectApi projectApi;

	protected ProjectApi getProjectApi() {
		if (projectApi == null) {
			projectApi = getGitlabApi().getProjectApi();
		}
		return projectApi;
	}

	private GroupApi groupApi;

	protected GroupApi getGroupApi() {
		if (groupApi == null) {
			groupApi = getGitlabApi().getGroupApi();
		}
		return groupApi;
	}

	private IssuesApi issuesApi;

	protected IssuesApi getIssuesApi() {
		if (issuesApi == null) {
			issuesApi = getGitlabApi().getIssuesApi();
		}
		return issuesApi;
	}

	private CommitsApi commitsApi;

	protected CommitsApi getCommitsApi() {
		if (commitsApi == null) {
			commitsApi = getGitlabApi().getCommitsApi();
		}
		return commitsApi;
	}

	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	private MavenProject project;

	public MavenProject getMavenProject() {
		return project;
	}

	protected File ensureTargetDirectory(String dir) throws IOException {
		File targetDir = new File(getMavenProject().getBuild().getDirectory(), dir);
		if (! targetDir.exists()) {
			Files.createDirectories(targetDir.toPath());
		}
		return targetDir;
	}

	protected boolean isDefined(String prop) {
		return prop != null && prop.length() > 0;
	}
}
