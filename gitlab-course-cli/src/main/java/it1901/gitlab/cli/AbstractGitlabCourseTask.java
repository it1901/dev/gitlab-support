package it1901.gitlab.cli;

import it1901.gitlab.repo.RepoNameProvider;

public abstract class AbstractGitlabCourseTask extends AbstractGitlabTask {

    private RepoNameProvider groupRepoNameProvider;
    
    public AbstractGitlabCourseTask() {
    }
    
    public RepoNameProvider getGroupRepoNameProvider() {
        return groupRepoNameProvider;
    }
    
    public void setGroupRepoNameProvider(RepoNameProvider groupRepoNameProvider) {
        this.groupRepoNameProvider = groupRepoNameProvider;
    }
}
