package it1901.gitlab.cli;

import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.function.Predicate;

import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffEntry.ChangeType;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.revwalk.RevCommit;

import it1901.gitlab.graphql.CsvWriter;

public class GitCommitsCsvWriter extends CsvWriter {
    
    private String[] headers = {
        "groupId", "committerId", "commitId", "commitDate", "kind", "path", "sizeMeasure"
    };
    
    @Override
    protected String[] getHeaders() {
        return headers;
    }
    
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");

    @Override
    protected void writeCsvRows(OutputStream output) {
    	throw new UnsupportedOperationException();
    }
    
    // should we limit diff measures to specific file types, e.g. java, md, txt, fxml, ...?
	private Predicate<String> diffPathFilter = null;

    void writeCsvRows(String groupId, RevCommit commit, List<DiffEntry> diffs, DiffFormatter diffFormatter, OutputStream output) {
    	for (DiffEntry diff: diffs) {
    		String[] entries = new String[headers.length];
    		entries[0] = groupId;
    		entries[1] = getCommitterId(commit);
    		entries[2] = commit.getName();
    		entries[3] = dateFormat.format(commit.getCommitterIdent().getWhen());
    		ChangeType changeType = diff.getChangeType();
			entries[4] = changeType.toString();
    		String path = diff.getNewPath();
    		if (changeType == ChangeType.DELETE) {
    			path = diff.getOldPath();
    		}
    		entries[4] = changeType.toString();
    		entries[5] = path;

    		int diffSizeMeasure = (diffPathFilter == null || diffPathFilter.test(path) ? getDiffSizeMeasure(diffFormatter, diff) : -1);
			entries[6] = String.valueOf(diffSizeMeasure);

    		getCsvWriter().writeRow(entries);
    	}
    }
    
	private String getCommitterId(RevCommit revCommit) {
    	String committerId = revCommit.getCommitterIdent().getEmailAddress();
    	String suffix = "@stud.ntnu.no";
    	if (committerId.endsWith(suffix)) {
    		committerId = committerId.substring(0, committerId.length() - suffix.length());
    	}
    	return committerId;
    }
	
	private DiffMeasureComputer diffMeasureComputer = null;
	
	public DiffMeasureComputer getDiffMeasureComputer() {
		return diffMeasureComputer;
	}
	
	public void setDiffMeasureComputer(DiffMeasureComputer diffMeasureComputer) {
		this.diffMeasureComputer = diffMeasureComputer;
	}
	
	private int getDiffSizeMeasure(DiffFormatter diffFormatter, DiffEntry diff) {
		try {
			return (diffMeasureComputer != null ? diffMeasureComputer.computeDiffMeasure(diffFormatter, diff) : -1);
		} catch (Exception e) {
			return -1;
		}
	}

// sample formattedDiff
//
//	diff --git a/StockTrader/fxui/src/main/java/gui/BuyStocksController.java b/StockTrader/fxui/src/main/java/gui/BuyStocksController.java
//	index 8c879a2..621b552 100644
//	--- a/StockTrader/fxui/src/main/java/gui/BuyStocksController.java
//	+++ b/StockTrader/fxui/src/main/java/gui/BuyStocksController.java
//	@@ -86,2 +86,6 @@
//	 
//	+    /**
//	+     * Get method for chosenStockName
//	+     * @return String
//	+     */
//	     public static String getChosenStockName() {
//	@@ -183,3 +187,3 @@
//	             userCash.setFill(Color.GREEN);
//	-            userCash.setText(InterfaceLogic.moneyInTheBank(usernameCurrent.getText())+ " $");
//	+            userCash.setText(InterfaceLogic.moneyInTheBank(usernameCurrent.getText()) + " $");
//	         } catch (IOException e) {
//	@@ -340,3 +344,3 @@
//	         userCash.setFill(Color.GREEN);
//	-        userCash.setText(InterfaceLogic.moneyInTheBank(usernameCurrent.getText())+ " $");
//	+        userCash.setText(InterfaceLogic.moneyInTheBank(usernameCurrent.getText()) + " $");
//	 
//	diff --git a/StockTrader/fxui/src/main/java/gui/UserAccountController.java b/StockTrader/fxui/src/main/java/gui/UserAccountController.java
//	index a3d3e1c..5a2f3b4 100644
//	--- a/StockTrader/fxui/src/main/java/gui/UserAccountController.java
//	+++ b/StockTrader/fxui/src/main/java/gui/UserAccountController.java
}
