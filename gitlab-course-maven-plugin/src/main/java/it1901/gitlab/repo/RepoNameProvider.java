package it1901.gitlab.repo;

public interface RepoNameProvider {

    public String getRepoName(String groupId);
}
