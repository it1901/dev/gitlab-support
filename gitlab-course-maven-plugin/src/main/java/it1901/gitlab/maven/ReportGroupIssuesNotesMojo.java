package it1901.gitlab.maven;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import it1901.gitlab.cli.ReportGroupIssuesNotesTask;

/**
 * Reports issues' notes as CSV
 */
@Mojo(name = "report-group-issues-notes")
public class ReportGroupIssuesNotesMojo extends AbstractGitlabMojo {

    @Parameter(required = true)
    private String groupsCsvPath;

    @Parameter(required = true)
    private String courseGroupsGroupsOwnerPath;
    
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
    	ReportGroupIssuesNotesTask.main(
			"-gitlabInstance", getGitlabInstance(),
			"-gitlabPersonalAccessToken", getGitlabPersonalAccessToken(),
			"-courseProjectPath", getMavenProject().getBuild().getDirectory(),
			"-groupsCsvPath", groupsCsvPath,
			"-courseGroupsGroupsOwnerPath", courseGroupsGroupsOwnerPath
		);
    }
}
