package it1901.gitlab.graphql;

import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Function;
import java.util.function.Predicate;

import org.gitlab4j.api.models.Note;

public class IssuesCsvWriter extends CsvWriter {

    private Collection<ProjectQuery.Issue> issues;
    
    public IssuesCsvWriter(Collection<ProjectQuery.Issue> issues) {
        this.issues = new ArrayList<ProjectQuery.Issue>(issues);
    }
    public IssuesCsvWriter(ProjectQuery.Issue... issues) {
        this.issues = Arrays.asList(issues);
    }        
    
    private String[] headers = {
        "issueNum", "userName", "entryKind", "timestamp", "text"
    };
    
    @Override
    protected String[] getHeaders() {
        return headers;
    }
    
    public static enum IssueNoteKind {
        CREATION, ASSIGNEMENT, COMMENT, COMMIT_REF, CLOSING
    }
    
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");

    private static String commitKindPrefix = "mentioned in commit ";
    private static Predicate<String> commitKindPredicate = s -> s.startsWith(commitKindPrefix);
    private static Function<String, String> commitKindBodyProvider = s -> s.substring(commitKindPrefix.length());
    
    private static String closedKindString = "closed";
    private static Predicate<String> closedKindPredicate = s -> s.equals(closedKindString);
    private static Function<String, String> closedKindBodyProvider = s -> null;

    private static String assignedKindPrefix = "assigned to ";
    private static Predicate<String> assignedKindPredicate = s -> s.startsWith(assignedKindPrefix);
    private static Function<String, String> assignedKindBodyProvider = s -> s.substring(assignedKindPrefix.length());

    @Override
    protected void writeCsvRows(OutputStream output) {
        for (ProjectQuery.Issue issue : issues) {
          String[] entries = new String[headers.length];
          entries[0] = String.valueOf(issue.iid);
          entries[1] = issue.author.getUsername();
          entries[2] = IssueNoteKind.CREATION.name();
          entries[3] = issue.createdAt;
          entries[4] = issue.title;
          getCsvWriter().writeRow(entries);
          for (Note note : issue.notes.nodes) {
              entries = new String[headers.length];
              entries[0] = String.valueOf(issue.iid);
              entries[1] = note.getAuthor().getUsername();
              String kind = IssueNoteKind.COMMENT.name();
              String body = note.getBody();
              if (commitKindPredicate.test(body)) {
                  kind = IssueNoteKind.COMMIT_REF.name();
                  body = commitKindBodyProvider.apply(body);
              } else if (closedKindPredicate.test(body)) {
                  kind = IssueNoteKind.CLOSING.name();
                  body = closedKindBodyProvider.apply(body);
              } else if (assignedKindPredicate.test(body)) {
                  kind = IssueNoteKind.ASSIGNEMENT.name();
                  body = assignedKindBodyProvider.apply(body);
              }
              entries[2] = kind;
              entries[3] = dateFormat.format(note.getCreatedAt());
              entries[4] = body;
              getCsvWriter().writeRow(entries);
          }
        }
    }
}
