package it1901.gitlab.cli;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gitlab4j.api.CommitsApi;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GroupApi;
import org.gitlab4j.api.IssuesApi;
import org.gitlab4j.api.ProjectApi;

import picocli.CommandLine;

public abstract class AbstractGitlabTask implements Runnable {
	
	private Log logger;
	
	public Log getLog() {
		if (logger == null) {
			logger = LogFactory.getLog(getClass());
		}
		return logger;
	}

	/**
	 * URL of gitlab instance.
	 */
	@CommandLine.Option(names = "-gitlabInstance", description = "base URL for gitlab instance, e.g. https://gitlab.stud.idi.ntnu.no")
	private String gitlabInstance;

	public String getGitlabInstance() {
		return gitlabInstance;
	}

	/**
	 * Personal Access Token (very secret, don't share)
	 */
	@CommandLine.Option(names = "-gitlabPersonalAccessToken", description = "Personal Access Token (very secret, don't share)")
	private String gitlabPersonalAccessToken;

	public String getGitlabPersonalAccessToken() {
		return gitlabPersonalAccessToken;
	}

	private GitLabApi gitLabApi;

	protected GitLabApi getGitlabApi() {
		if (gitLabApi == null) {
			gitLabApi = new GitLabApi(gitlabInstance, gitlabPersonalAccessToken);
		}
		return gitLabApi;
	}

	private ProjectApi projectApi;

	protected ProjectApi getProjectApi() {
		if (projectApi == null) {
			projectApi = getGitlabApi().getProjectApi();
		}
		return projectApi;
	}

	private GroupApi groupApi;

	protected GroupApi getGroupApi() {
		if (groupApi == null) {
			groupApi = getGitlabApi().getGroupApi();
		}
		return groupApi;
	}

	private IssuesApi issuesApi;

	protected IssuesApi getIssuesApi() {
		if (issuesApi == null) {
			issuesApi = getGitlabApi().getIssuesApi();
		}
		return issuesApi;
	}

	private CommitsApi commitsApi;

	protected CommitsApi getCommitsApi() {
		if (commitsApi == null) {
			commitsApi = getGitlabApi().getCommitsApi();
		}
		return commitsApi;
	}

	@CommandLine.Option(names = "-courseProjectPath", description = "course project path")
	private Path courseProjectPath;

	public Path getCourseProjectPath() {
		return courseProjectPath;
	}

	public Path getCourseProjectTargetPath() {
		return courseProjectPath.resolve("target");
	}

	protected Path ensureTargetDirectory(String dir) throws IOException {
		Path targetDir = getCourseProjectTargetPath().resolve(dir);
		if (! Files.exists(targetDir)) {
			Files.createDirectories(targetDir);
		}
		return targetDir;
	}

	protected static boolean isDefined(String prop) {
		return prop != null && prop.length() > 0 && ! (prop.equals("null"));
	}
}
