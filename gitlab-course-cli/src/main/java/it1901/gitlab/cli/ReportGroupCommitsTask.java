package it1901.gitlab.cli;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Commit;

import it1901.gitlab.course.Group;
import it1901.gitlab.repo.RepoNameProvider;
import it1901.gitlab.repo.TimeBasedRepoNameProvider;
import picocli.CommandLine;

/**
 * Reports issues' notes as CSV
 */
public class ReportGroupCommitsTask extends AbstractGitlabGroupTask {
    
	@CommandLine.Option(names = "-commitedSince", description = "limits group projects to those with commits since this date")
	private String commitedSince;

	@CommandLine.Option(names = "-commitedUntil", description = "limits group projects to those with commits before this date")
	private String commitedUntil;
    
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	private Date string2Date(String dateString) {
		try {
			return dateFormat.parse(dateString);
		} catch (ParseException e) {
			return null;
		}
	}

    @Override
    public void run() throws TaskException {
        Map<String, Group> groups = readGroupsCsv(false);
        RepoNameProvider repoNameProvider = getRepoNameProvider();
		Date since = (isDefined(commitedSince) ? string2Date(commitedSince) : null);
		Date until = (isDefined(commitedUntil) ? string2Date(commitedUntil) : null);
        for (String groupId : groups.keySet()) {
            String repoName = repoNameProvider.getRepoName(groupId);
            String repoPath = getCourseGroupsGroupsOwnerPath() + File.separator + repoName + File.separator + repoName;
            List<Commit> commits = null;
			try {
				getLog().info("Getting commits for " + repoName + " @ " + repoPath);
				commits = getCommitsApi().getCommits(repoPath, null, since, until, null, true, true, null);
			} catch (GitLabApiException e) {
				getLog().error(e);
			} catch (Exception e) {
				getLog().error(e);
			}
			if (commits == null) {
				continue;
			}
            GitlabCommitsCsvWriter csvWriter = new GitlabCommitsCsvWriter(commits);
            try {
                File file = ensureTargetDirectory("gitlab").resolve(repoName + "-commits.csv").toFile();
                getLog().info("Writing " + commits.size() + " commits to " + file.getName() + " @ " + file.getParent());
				try (OutputStream output = new FileOutputStream(file)) {
                    csvWriter.writeCsv(output);
                    getLog().info("Wrote csv for " + groupId + " in " + repoName);
                } catch (IOException ioe) {
                    getLog().info("Couldn't write csv for group " + groupId + " in " + repoName + ": " + ioe.getMessage());
                } catch (Exception e) {
                	e.printStackTrace(System.out);
                	getLog().info("Couldn't write csv for group " + groupId + " in " + repoName + ": " + e.getMessage());
                }
            } catch (IOException ioe) {
                throw new TaskException("Couldn't create build directory for group " + groupId + ": " + ioe.getMessage());
            }
        }
    }
    
	public static void main(final String... args) {
		new CommandLine(new ReportGroupCommitsTask()).execute(args);
	}
}
