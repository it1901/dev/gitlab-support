package it1901.gitlab.repo;

@FunctionalInterface
public interface RepoNameProvider {

    public String getRepoName(String groupId);
}
