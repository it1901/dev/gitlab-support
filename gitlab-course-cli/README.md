# CLI for gathering data from and managing gitlab-supported courses

Many courses use gitlab for supporting courses, where students work individually, in pairs or in groups and
manage their artifacts in gitlab repos within course groups (or some structure rooted in a course (instance)).

## Tasks and configuration options

This repo contains support for operating on such repos using a CLI based on Java classes
with [picocli](https://picocli.info) annotations.
Each such class performs a task that can be configured using various command line options.
Some options are pretty generic, e.g. all tasks that access the gitlab server must include the `-gitlabInstance` option.
Some are more specific, e.g. tasks may use commit time as filter and use the `-commitedSince` and `-commitedUntil options`.

Tasks are executed by running the corresponding Java class with options (name and value) as program arguments.
In the terminal, this is done with `java <qualified-task-class-name> <...options...>`.
In an IDE, you typically create some kind of launch configuration and provide program arguments in a form.

## Executing tasks with maven

To execute tasks as part of a build, you configure one or more maven `exec:java` tasks in a `pom.xml` file.
The arguments are included in a set of `argument` elements in the `configuration` of an `execution`.
You can then link the task to a lifecycle phase or call it manually using `mvn exec:java@execution-id`.

Note that to access the gitlab instance, you usually need to include a so-call personal access token as the
`-gitlabPersonalAccessToken` option. This token should not be stored in plain text in a repo.
The best is perhaps to include it in the command line with something like `"-DgPAT=your-personal-access-token".`
Here it is assumed that the pom.xml uses the `gPAT` property as the `-gitlabPersonalAccessToken` option.

The following is a sample plugin declaration for executing the `ReportGroupIssuesNotesTask`.
Note that the `executions` element may include more than one `execution` element.

```
	<plugin>
		<groupId>org.codehaus.mojo</groupId>
		<artifactId>exec-maven-plugin</artifactId>
		<version>3.0.0</version>
		<executions>
			<execution>
				<id>report-group-issues-notes</id>
				<goals>
					<goal>java</goal>
				</goals>
				<configuration>
					<mainClass>it1901.gitlab.cli.ReportGroupIssuesNotesTask</mainClass>
					<arguments>
						<argument>-gitlabInstance</argument>
						<argument>${gitlabInstance}</argument>

						<argument>-gitlabPersonalAccessToken</argument>
						<argument>${gPAT}</argument>

						<argument>-courseProjectPath</argument>
						<argument>${project.basedir}</argument>

						<argument>-courseGroupsGroupsOwnerPath</argument>
						<argument>${groupGroupsGroup}</argument>

						<argument>-groupsCsvPath</argument>
						<argument>${groupsCsvPath}</argument>

						<argument>-includeGroupIds</argument>
						<argument>1,2,3,4,5,6,7,8,9</argument>					

						<argument>-repoNameFormat</argument>
						<!-- use xml:space="preserve" for string formats with whitespace -->
						<argument xml:space="preserve">gr20%02d</argument>
					</arguments>
				</configuration>
			</execution>
		</executions>
	</plugin>
```