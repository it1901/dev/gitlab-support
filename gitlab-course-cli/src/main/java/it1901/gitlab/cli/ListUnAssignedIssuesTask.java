package it1901.gitlab.cli;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.Pager;
import org.gitlab4j.api.models.Group;

import picocli.CommandLine;

/**
 * Lists student projects
 */
public class ListUnAssignedIssuesTask extends AbstractGitlabGroupTask {

  @Override
  public void run() throws TaskException {
    getLog().info("Getting unassigned issues in " + getCourseGroupsGroupsOwnerPath());
    try {
      final Pager<Group> projects = getGroupApi().getSubGroups(getCourseGroupsGroupsOwnerPath(), 50);
      final List<String[]> groupNameAndPathList = projects.lazyStream().map(group -> new String[]{group.getName(), group.getFullPath()}).collect(Collectors.toList());
      getLog().info("Found " + groupNameAndPathList.size() + " groups in " + getCourseGroupsGroupsOwnerPath() + ": ");
      final List<String[]> issueNumberAndPathList = new ArrayList<>();
      groupNameAndPathList.parallelStream().forEach(sa -> {
        final String projectPath = sa[1] + "/" + sa[0];
        try {
          getIssuesApi().getIssues(projectPath, 50).stream().forEach(issue -> {
            if (issue.getAssignee() == null) {
              System.out.println("Issue #" + issue.getIid() + " in " + projectPath + " is not assigned");
              issueNumberAndPathList.add(new String[] {String.valueOf(issue.getIid()), projectPath});
            }
          });
        } catch (IllegalStateException | GitLabApiException e) {
          System.err.println("Exception getting issues for " + projectPath + ": " + e);
        }
      });
      System.out.println("# of unassigned issues: " + issueNumberAndPathList.size());
    } catch (final Exception e) {
      throw new TaskException("Exception getting groups names", e);
    }
  }
  
	public static void main(final String... args) {
		new CommandLine(new ListUnAssignedIssuesTask()).execute(args);
	}
}
