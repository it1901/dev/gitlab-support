package it1901.gitlab.cli;

import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;

public interface DiffMeasureComputer {

	int computeDiffMeasure(DiffFormatter diffFormatter, DiffEntry diff) throws Exception;
}
