package it1901.gitlab.maven;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import it1901.gitlab.cli.ListUnAssignedIssuesTask;

/**
 * Lists student projects
 */
@Mojo(name = "list-unassigned-issues")
public class ListUnAssignedIssuesMojo extends AbstractGitlabMojo {

  @Parameter(required = true)
  private String courseGroupsGroupsOwnerPath;

  @Override
  public void execute() throws MojoExecutionException {
	  ListUnAssignedIssuesTask.main(
		"-gitlabInstance", getGitlabInstance(),
		"-gitlabPersonalAccessToken", getGitlabPersonalAccessToken(),
		"-courseProjectPath", getMavenProject().getBuild().getDirectory(),
		"-courseGroupsGroupsOwnerPath", courseGroupsGroupsOwnerPath
		);
  }
}
